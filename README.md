# Predictive Modelling of Physical Spaces

## Requirements
- Python
- Numpy
- Scipy
- Munkres
- Imutils 
- Pylab

## Basic Detection and Tracking Usage With MOG Background Subtraction

```python main.py <options> ```

Command Line Options

* -v <filepath> for to choose video source 
* -m <"mog"> for MOG method or <"ext"> to background modelling
* -s <"True"> to select region to be tracked or <"False"> for none
* -b <filepath> for background image  


### Basic analysis usage

```python analyse.py <options>```


* -f <file_path> Source of tracks
* -b <file_path> Background Image
* -g <"all"> for all tracks or <"filtered> for pre-processed tracks


