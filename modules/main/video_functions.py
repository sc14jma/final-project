import yaml
import numpy as np
import cv2


def undistort(frame):
	h, w = frame.shape[:2]
	try :
		with open("data.yaml") as fil:
			data = yaml.load(fil)
	except yaml.YAMLError as e:
		print e
		return False, frame

	mtx = np.array(data["camera_matrix"])
	dist = np.array(data["dist_coeff"])

	newcameramtx , roi = cv2.getOptimalNewCameraMatrix(mtx,dist,(w,h),1,(w,h))

	dst = cv2.undistort(frame, mtx,dist, None, newcameramtx)


	x,y,w,h = roi
	dst = dst[y:y+h, x:x+w]
	return True, dst
